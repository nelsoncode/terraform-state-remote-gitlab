resource "local_file" "ip_address" {
  content  = digitalocean_droplet.mydroplet.ipv4_address
  filename = "ip_address.txt"
}
