PROJECT_ID=""
STATE_NAME="my-state"
GITLAB_USERNAME=""
GITLAB_PASSWORD="" # GITLAB PERSONAL ACCES TOKEN
TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}"

terraform init \
  -backend-config=address=${TF_ADDRESS} \
  -backend-config=lock_address=${TF_ADDRESS}/lock \
  -backend-config=unlock_address=${TF_ADDRESS}/lock \
  -backend-config=username=${GITLAB_USERNAME} \
  -backend-config=password=${GITLAB_PASSWORD} \
  -backend-config=lock_method=POST \
  -backend-config=unlock_method=DELETE \
  -backend-config=retry_wait_min=5 \
  -var-file=".env"